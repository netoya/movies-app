## Movies App

A react-native example for show movies information.


## Setup

### Clone

```
git clone https://gitlab.com/netoya/movies-app.git
```

### Install dependencies
```
npx yarn install
npx pod-install ios 
```

### 
```
cp .config.example.js .config.js
```

Edit omdbapiKey
```javascript
export const omdbapiKey = "PASTE YOUR KEY HERE";
```

## Run

### IOS
``` 
npm run ios
```

### Android
``` 
npm run android
```