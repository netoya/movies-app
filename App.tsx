import React, { useEffect, useMemo, useRef, useState } from 'react'
import { View, Text, StatusBar, SafeAreaView, TouchableOpacity, FlatList, TextInput } from 'react-native'


import RootNavigation from './app/root/RootNavigation';

const App = () => {

  return (
    <SafeAreaView className="flex-1 bg-orange-700">
      <StatusBar />
      <RootNavigation />
    </SafeAreaView>
  )
}

export default App
