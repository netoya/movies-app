export interface TSearchData {
    Search: any[] | undefined
}


export interface TSearchItemData {
    imdbID: string;
    Ratings: TSearchRating[];
    Plot: string;
    Title: String;
    Poster: String;
}


export interface TSearchRating {
    Source: string;
    Value: string;
}