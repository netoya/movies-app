import React, { useEffect, useRef, useState } from 'react'
import { View, Text, TextInput } from 'react-native'
import { MovieList } from '../components/MovieList'


const MoviesScreen = () => {
    const [searchText, setSearchText] = useState("")
    const [ready, setReady] = useState(false)
    const timeOutRef = useRef(0)

    useEffect(() => {
        clearTimeout(timeOutRef.current)
        setReady(false)
        timeOutRef.current = setTimeout(() => {

            setReady(true)
        }, 1000)

        return () => {
            clearTimeout(timeOutRef.current)
        }

    }, [searchText])



    return (

        <View className='flex-1 bg-orange-700'>

            <View className='p-2'>
                <Text className='text-3xl my-4 text-center font-extrabold tracking-widest text-white' style={
                    { textShadowColor: "rgba(0,0,0,0.75)", textShadowOffset: { width: -1, height: 1 }, textShadowRadius: 1 }
                }>
                    Movies App
                </Text>
                <TextInput className='bg-white h-10 rounded px-2 shadow-sm shadow-black' onChangeText={(text) => setSearchText(text)}></TextInput>
            </View>
            <MovieList search={searchText} ready={ready} />
        </View>
    )
}

export default MoviesScreen
