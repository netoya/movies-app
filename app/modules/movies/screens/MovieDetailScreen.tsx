import React from 'react'
import { View, Text } from 'react-native'
import { MovieItem } from '../components/MovieItem'
import { useRoute } from '@react-navigation/native';
import { TSearchData } from '../handlers/interfaces';


const MovieDetailScreen = () => {

    const route = useRoute()

    const { params } = route

    // 
    const { item }: { item: TSearchData } = params


    return (
        <View className='flex-1 bg-orange-700'>

            <View className='p-2'>
                <MovieItem item={item} plot={'full'} />
            </View>
        </View>
    )
}

export default MovieDetailScreen
