import React, { memo, useEffect, useState } from 'react';
import { View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { TSearchItemData } from '../handlers/interfaces';

import { omdbapiKey } from "./../../../../.config";

export const MovieItem = memo(({ item, plot = "short" }: { item: TSearchItemData | any; plot?: string | undefined }) => {

    const [loading, setLoading] = useState(false);
    const [data, setData] = useState<TSearchItemData | undefined>();

    const { imdbID = "" } = item;
    if (!imdbID) { return; }

    const navigation = useNavigation()

    useEffect(() => {
        if (!!imdbID) {
            getData();
        }
    }, [imdbID]);

    const getData = async () => {
        setLoading(true);
        let data: TSearchItemData = await fetch(`https://www.omdbapi.com/?i=${imdbID}&apikey=${omdbapiKey}&plot=${plot}`)
            .then(resp => resp.json());


        if (data.Poster == "N/A" ) {
            // via.placeholder.com with text "No Image"
            data.Poster = "https://via.placeholder.com/300x450?text=No+Image";
        }
        setLoading(false);


        setData(data);
    };


    if (loading || !data) {
        return <View className={`shadow-sm shadow-black relative bg-white rounded-lg flex flex-col p-2 items-center justify-center ${plot == "short" ? "h-64" : "h-52 mb-4"}`}>

            <ActivityIndicator size={'large'}></ActivityIndicator>
        </View>;
    }



    return (<>
        <TouchableOpacity className={`shadow-sm shadow-black relative bg-white rounded-lg flex flex-col p-2 ${plot == "short" ? "h-64" : "h-52 mb-4"}`} onPress={() => {
            navigation.navigate("MovieDetail", { item: data })
        }}
            disabled={plot != "short"}>
            <Image className='absolute top-2  w-full h-32 rounded mx-2 rounded' source={{ uri: data?.Poster != "N/A" ? data?.Poster : undefined }} />
            <View className='relative flex flex-col justify-end  bg-black/60 h-32 rounded mb-1'>
                <View className='flex h-20 justify-center items-center px-8'>

                    <Text className='text-white text-xl flex-2x1 font-bold text-center  tracking-widest'>{data?.Title}</Text>
                </View>
            </View>
            <View className='flex-1'>
                <View className='flex flex-row flex-1 mb-4'>
                    {data.Ratings.map(rating => (
                        <View key={rating.Source} className='w-1/3 flex h-14 justify-between'>
                            <Text className='text-center align-middle'>{rating.Source}</Text>
                            <Text className='text-center align-middle'>{rating.Value}</Text>
                        </View>
                    ))}
                </View>
                {plot == "short" && <View className={'flex flex-row h-12'}>
                    <Text className='flex-1 italic'>{data?.Plot.substr(0, 150)}</Text>
                </View>}
            </View>
        </TouchableOpacity>
        {plot != "short" && <View className='shadow-sm shadow-black relative bg-white rounded-lg flex flex-col p-2'>
            <Text>{data?.Plot}</Text>
        </View>}
    </>);
});
