import { FlashList } from '@shopify/flash-list';
import React, { memo, useEffect, useState } from 'react';
import { View } from 'react-native';
import { TSearchData, TSearchItemData } from '../handlers/interfaces';
import { MovieItem } from './MovieItem';

import { omdbapiKey } from "./../../../../.config";

export const MovieList = memo(({ search, ready }: { search: string; ready: boolean }) => {

    const [data, setData] = useState<TSearchData | undefined>();


    const [page, setPage] = useState(1);

    useEffect(() => {
        if (!!search && !!ready) {
            setPage(1);
        } else {
            setData(undefined);
            setPage(0);
        }
    }, [search, ready]);

    useEffect(() => {
        if (!!page) {
            getData(search, page);
        }
    }, [page]);

    const getData = async (search: string, page: number) => {



        let data = await fetch(`https://www.omdbapi.com/?s=${search}&page=${page}&apikey=${omdbapiKey}`, {
            method: "POST"
        }).then(resp => resp.json());



        setData((curr) => {
            let { Search = [] } = curr || {};
            Search = Search.concat(data.Search || []);
            return { Search };
        });
    };

    const renderItem = ({ item }: { item: TSearchItemData }) => {
        return <View className="p-2"><MovieItem item={item}></MovieItem></View>;
    };

    if (!page) {
        return null;
    }



    return (
        <FlashList
            data={data?.Search || []}
            renderItem={renderItem}
            estimatedItemSize={500}
            onEndReached={() => setPage(page + 1)}
            onEndReachedThreshold={0.7}
            keyExtractor={(item) => item.imdbID} />
    );
});
