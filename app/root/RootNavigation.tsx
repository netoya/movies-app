import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MoviesScreen from '../modules/movies/screens/MoviesScreen';
import MovieDetailScreen from '../modules/movies/screens/MovieDetailScreen';


const Stack = createNativeStackNavigator();

function RootNavigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{
                headerShown: false

            }}>
                <Stack.Screen name="Movies" component={MoviesScreen} />
                <Stack.Screen name="MovieDetail" component={MovieDetailScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default RootNavigation;